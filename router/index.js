import express from "express";

export const router = express.Router();

export default { router };

// Configurar primera ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "Examen 02 " });
});

router.get("/colegio", (req, res) => {
  const params = {
    titulo: "Examen 02",
    numDocente: req.query.numDocente,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    nivel: req.query.nivel,
    pagoBasePorHora: req.query.pagoBasePorHora,
    horasImpartidas: req.query.horasImpartidas,
    cantidadHijos: req.query.cantidadHijos,
    isPost: false,
  };
  res.render("colegio", params);
});

router.post("/colegio", (req, res) => {
  const { numDocente, nombre, domicilio, nivel, pagoBasePorHora, horasImpartidas, cantidadHijos } = req.body;

  const pagoBase = calcularPagoBase(nivel, pagoBasePorHora);
  const pagoTotal = calcularPagoTotal(pagoBase, horasImpartidas);
  const impuesto = calcularImpuesto(pagoTotal);
  const bonoPaternidad = calcularBonoPaternidad(cantidadHijos, pagoTotal);
  const pagoFinal = calcularPagoFinal(pagoTotal, impuesto, bonoPaternidad);

  const params = {
    titulo: "Examen 02 ",
    numDocente,
    nombre,
    domicilio,
    nivel,
    pagoBasePorHora,
    horasImpartidas,
    cantidadHijos,
    pagoBase,
    pagoTotal,
    impuesto,
    bonoPaternidad,
    pagoFinal,
    isPost: true,
  };

  res.render("colegio", params);
});

// Función para calcular el pago base
function calcularPagoBase(nivel, pagoBasePorHora) {
  if (nivel === "1") {
    return pagoBasePorHora * 1.3; // Incremento del 30% para nivel 1
  } else if (nivel === "2") {
    return pagoBasePorHora * 1.5; // Incremento del 50% para nivel 2
  } else if (nivel === "3") {
    return pagoBasePorHora * 2;   // Incremento del 100% para nivel 3
  } else {
    return null;
  }
}

// Función para calcular el pago total
function calcularPagoTotal(pagoBase, horasImpartidas) {
  return pagoBase * horasImpartidas;
}

// Función para calcular el impuesto
function calcularImpuesto(pagoTotal) {
  return pagoTotal * 0.16; // Impuesto del 16% sobre el pago total
}

// Función para calcular el bono por paternidad
function calcularBonoPaternidad(cantidadHijos, pagoTotal) {
  if (cantidadHijos === "1" || cantidadHijos === "2") {
    return pagoTotal * 0.05; // Bono del 5% para 1 o 2 hijos
  } else if (cantidadHijos >= "3" && cantidadHijos <= "5") {
    return pagoTotal * 0.10; // Bono del 10% para 3 a 5 hijos
  } else if (cantidadHijos > "5") {
    return pagoTotal * 0.20; // Bono del 20% para más de 5 hijos
  } else {
    return 0; // Sin bono para 0 hijos
  }
}

function calcularPagoFinal(pagoTotal, impuesto, bonoPaternidad) {
  return pagoTotal + bonoPaternidad - impuesto;
}
